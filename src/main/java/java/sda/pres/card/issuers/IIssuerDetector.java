package java.sda.pres.card.issuers;

public interface IIssuerDetector {
    String detectIssuer(String cardNo, String filePath);
}
