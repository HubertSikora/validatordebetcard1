package java.sda.pres.card.issuers.impl;

import java.sda.pres.card.issuers.IRuleBuilder;
import java.sda.pres.card.issuers.IssuerRule;
import java.util.List;
import java.util.Map;

public class IssuerRuleBuilderFactory {

    public static List<Map<String, String>> produce(String filePath) {
        IRuleBuilder rulesBuilder = null;
        if (filePath != null && !filePath.isEmpty()) {
            rulesBuilder = new IssuerRuleFromFileBuilderTxt(filePath);
        } else {
            rulesBuilder = new IssuerRuleBuilder();
        }

        return rulesBuilder.buildRules();
    }
}
