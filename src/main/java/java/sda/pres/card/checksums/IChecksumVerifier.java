package java.sda.pres.card.checksums;

public interface IChecksumVerifier {
    boolean verify(String cardNo);
}
