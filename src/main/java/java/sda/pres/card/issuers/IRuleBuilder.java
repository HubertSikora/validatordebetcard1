package java.sda.pres.card.issuers;



import java.util.List;
import java.util.Map;

public interface IRuleBuilder {
    List<Map<String, String>> buildRules();
}
