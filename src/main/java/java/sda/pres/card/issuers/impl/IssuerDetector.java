package java.sda.pres.card.issuers.impl;


import java.sda.pres.card.issuers.IIssuerDetector;
import java.sda.pres.card.issuers.IRuleBuilder;
import java.sda.pres.card.issuers.IssuerRule;
import java.util.List;
import java.util.Map;

public class IssuerDetector implements IIssuerDetector {

    @Override
    public String detectIssuer(String cardNo, String filePath) {
        String result = "UNKNOWN";


// todo fabryka ma zwracać list<Map<String, String>> zamiast issuerRule

        List<Map<String, String>> rulesList = IssuerRuleBuilderFactory.produce(filePath);
        //   List<IssuerRule> rulesList = IssuerRuleBuilderFactory.produce(filePath);

        /*
                IRuleBuilder rulesBuilder = null;
        if (filePath != null && !filePath.isEmpty()) {
            rulesBuilder = new IssuerRuleFromFileBuilderTxt(filePath);
        } else {
            rulesBuilder = new IssuerRuleBuilder();
        }

        List<IssuerRule> issuerRules = rulesBuilder.buildRules();
*/
        // algorytm przyrównuje każdą regułę do stringa cardNo przekazanego jako parametr
        // jeśli uda się dopasować regułę (program "wejdzie w ifa"), do zmiennej result
        // zostanie przypisana wartość name z obiektu reprezentującego regułę

        /**
         * for (int i = 0; i < issuerRules.size();i++) {
         *     IssuerRule issuerRule = issuerRules.get(i);
         * }
         */
        IssuerRuleFromFileBuilderTxt aaa = new IssuerRuleFromFileBuilderTxt(filePath);
        for (Map<String, String> issuerRule : rulesList) {
            //if (cardNo.startsWith( String.valueOf( rulesList.getPrefix())) && cardNo.length() == issuerRule.getLength()) {
            if (cardNo.startsWith(issuerRule.get("prefix")) &&  issuerRule.get("length").equals(String.valueOf(cardNo.length())) ) {
                //result = issuerRule.getIssuerName();
                result = issuerRule.get("name");
                //
            }
        }
        return result;
    }
}
